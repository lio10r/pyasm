#!/usr/bin/python3
# PYTHON_ARGCOMPLETE_OK

import sys
import argparse

from enum import IntEnum, unique
from functools import lru_cache

VERSION = "1.0.0"

REGISTER_MASK = 0b111
WORD_SIZE = 2

# Binary representation -> name, number of params
OPCODES = {
    0b0000: ("mov", 2),
    0b0001: ("cmp", 2),
    0b0010: ("add", 2),
    0b0011: ("sub", 2),
    0b0100: ("not", 1),
    0b0101: ("clr", 1),
    0b0110: ("lea", 2),
    0b0111: ("inc", 1),
    0b1000: ("dec", 1),
    0b1001: ("jmp", 1),
    0b1010: ("jne", 1),
    0b1011: ("jz",  1),
    0b1100: ("xor", 2),
    0b1101: ("or",  2),
    0b1110: ("rol", 2),
    0b1111: ("nop", 0),
}

# Binary representation -< name
REGISTERS = {
    0: "r0",
    1: "r1",
    2: "r2",
    3: "r3",
    4: "r4",
    5: "r5",
    6: "r6",
    7: "r7",
}


@unique
class AddressingMode(IntEnum):
    REGISTER    = 0b00
    IMMEDIATE   = 0b01
    DIRECT      = 0b10
    INDIRECT    = 0b11
    

@lru_cache()
def format_address(addr: int) -> str:
    # Remove leading "0x", pad with "0", add "0x"
    return "0x" + hex(addr)[2:].rjust(4, "0")


def disassemble(file, print_addresses=False):
    address = 0x0000
    
    while True:
        # Read word
        try:
            first, second = file.read(WORD_SIZE)
            
        except:
            break
        
        else:
            if print_addresses:
                print(format_address(address), end=": ")
                
            address += WORD_SIZE
            
        opcode = first >> 4
        op_name, params = OPCODES[opcode]
        
        if params == 0:
            print(op_name)
            continue
            
        addressing_mode = AddressingMode(((first & 0b1) << 1) + ((second & 0b10000000) >> 7))
        
        if params == 1:
            if addressing_mode is AddressingMode.REGISTER:
                register = (first >> 1) & REGISTER_MASK
                var_name = REGISTERS[register]
                
            else:
                var_name = "0x" + file.read(WORD_SIZE).hex()
                address += WORD_SIZE
                
            if addressing_mode is AddressingMode.INDIRECT:
                var_name = "[{}]".format(var_name)
                
            print(op_name, var_name)
            
        elif params == 2:
            # First parameter must be register
            register = (first >> 1) & REGISTER_MASK
            register_name = REGISTERS[register]

            if addressing_mode is AddressingMode.REGISTER:
                second_register = (second >> 4) & REGISTER_MASK
                second_var_name = REGISTERS[second_register]

            else:
                second_var_name = "0x" + file.read(WORD_SIZE).hex()
                address += WORD_SIZE

            if addressing_mode is AddressingMode.INDIRECT:
                second_var_name = "[{}]".format(second_var_name)

            print(op_name, register_name, ",", second_var_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--version", help="Show version and exit",
                        action="version", version="BetterDisassembler {} @NivBro".format(VERSION))

    parser.add_argument("file", type=argparse.FileType("rb"), help="Binary file to process")

    parser.add_argument("--no-addresses", action="store_false", help="Don't print addresses")

    try:
        import argcomplete
    except ImportError:
        pass
    else:
        argcomplete.autocomplete(parser)

    args = parser.parse_args()

    disassemble(args.file, print_addresses=args.no_addresses)
